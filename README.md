# Leaflet.ImageOverlay.Arrugator

Displays reprojected raster images.

Leverages [Glii](https://gitlab.com/IvanSanchez/glii) for not going insane with the WebGL bits, and [Arrugator](https://gitlab.com/IvanSanchez/arrugator) for calculating the triangle mesh for raster reprojection.

### Demo

https://zvikabh.gitlab.io/Leaflet.ImageOverlay.Arrugator/demo.html

The demo linked above uses a downsampled version of the [N5000 raster dataset from Kartverket](https://kartkatalog.geonorge.no/metadata/n5000-raster/7e1b827e-3ccd-48a8-91ce-cc8125d6e4c1). That raster image is originally in EPSG:25833 (AKA ETRS89+UTM33N), and is seamlessly reprojected into EPSG:3857.

### Usage

This Leaflet plugin adds a `L.ImageOverlay.Arrugator` class and a `L.imageOverlay.arrugator` factory to the Leaflet namespace.

Load the plugin in a `<script>` tag, from a local copy or from a CDN, e.g.

```html
<script src="https://unpkg.com/leaflet.imageoverlay.arrugator@1.0.0/dist/leaflet.imageoverlay.arrugator.js"></script>
```

Usage is as follows:

```js
L.imageOverlay.arrugator(
    // First argument to the factory/constructor is the URL of the image. Only png/jpg.
    'demodata/Basisdata_0000_Norge_25833_N5000Raster_TIFF/33_N5000raster_1.png',
    {
        // The "controlPoints" option must be an array of arrays of numbers, containing
        // the coordinates in the source CRS of the four corners of the image, as follows:
        controlPoints: [
            [-183622.300, 7996344.000],	// top-left
            [-183622.300, 6396344.000],	// bottom-left
            [1416377.700, 7996344.000],	// upper-right
            [1416377.700, 6396344.000],	// lower-right
        ],

        // The "projector" option must be a forward-projection function.
        // Leveraging proj4 as follows is recommended.
        // It's up to the developer to ensure that the destination projection matches the Leaflet display CRS.
        projector: proj4('EPSG:25833','EPSG:3857').forward,

        // The "epsilon" option controls how much the triangular mesh will be subdivided.
        // Set it to the *square* of the maximum expected error, in units of the destination CRS.
        // The default of one million means that the maximum reprojection error distance shall be 1000 "meters".
        epsilon: 1000000,

        // If you don't know what a "fragment shader" is, do not change this default.
        // If you *do* know what a "fragment shader" is, then be aware that there's a
        // predefined `uRaster` 2D sampler and a `vUV` `vec2` varying.
        fragmentShader: "void main() { gl_FragColor = texture2D(uRaster, vUV); }"

        // Rasters that cover very large areas (i.e. the whole earth) can lead to
        // projection artifacts. For those cases, subdivide the mesh before
        // arrugating by providing a value larger than 1.
        subdivisions: 1,

        // If the input coordinates are so large, or so close to discontinuties/asimptotic
        // points, these options will crop it to prevent artifacts.
        // In other words: when the input data covers the poles, prevent
        // projecting the areas near the poles by cropping the `Y` coordinate
        // between `[-85.5, 85.5]`.
        cropX: [-Infinity, Infinity],
        cropY: [-Infinity, Infinity],

        // Can take a "padding" option, as per L.Renderer
        padding: 0.1,

        // "opacity" as per L.ImageOverlay
        opacity: 1,

        // Can take usual L.Layer options as well.
        attribution: "<a href='https://kartkatalog.geonorge.no/metadata/n5000-raster/7e1b827e-3ccd-48a8-91ce-cc8125d6e4c1'>N5000 by Kartverket</a>",
        pane: "overlayPane"
    }
).addTo(map);
```

### Gotchas

Even though the name of the plugin is `L.ImageOverlay.Arrugator`, this class does **not** inherit from `L.ImageOverlay` but, rather, implements parts of `L.Canvas` to support a glii-powered WebGL rendering.

This shouldn't be a problem, but is worth mentioning.

The epsilon for the arrugator triangulation process cannot be changed after instantiation (yet).

### Legalese

Released under the General Public License, v3. See the LICENSE file for details.
